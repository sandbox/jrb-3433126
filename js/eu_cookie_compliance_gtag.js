/**
 * @file
 * gtag-consent.js
 *
 * Integrates gtag consent with eu cookie compliance banner.
 */

(function (Drupal, $) {

  'use strict';

  let gtagConsent = 'denied';

  Drupal.behaviors.euCookieComplianceGTagConsent = {
    attach (context, settings) {

      let consentStatusHandler = function (response) {

        let setGTagConsent = function () {
          if (typeof gtag === 'function') {
            gtagConsent = consentValue;
            gtag('consent', 'update', {
              'ad_user_data': consentValue,
              'ad_personalization': consentValue,
              'ad_storage': consentValue,
              'analytics_storage': consentValue
            });
          }
          else {
            if (response.currentStatus !== '0') {
              setTimeout(setGTagConsent, 500);
            }
          }
        }

        let consentValue = (response.currentStatus > 0) ? 'granted' : 'denied';
        if (gtagConsent !== consentValue) {
          let timeOut = (response.currentStatus === null) ? 0 : 500;
          setTimeout(setGTagConsent, timeOut);
        }

      };

      Drupal.eu_cookie_compliance('postStatusSave', consentStatusHandler);

      let cookieConsentValue = {
        currentStatus: Drupal.eu_cookie_compliance.getCookieStatus()
      };
      consentStatusHandler(cookieConsentValue);

    }
  };

} (Drupal, jQuery));
