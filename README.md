# EU Cookie Compliance Google Tag Integration

This module integrates the EU Cookie Compliance module with the `gtag` script. It
will update the user's consent setting based on the value of the consent
cookie.

In order for this to work, the Google Tag Manager `gtag` script must be included
on the page by some other method. This can be done with a custom script or with
a module like [Google Tag](https://www.drupal.org/project/google_tag).

When the user consents to cookies, it will call gtag with this:
```javascript
gtag('consent', 'update', {
  'ad_user_data': 'granted',
  'ad_personalization': 'granted',
  'ad_storage': 'granted',
  'analytics_storage': 'granted'
});
```

## Usage

1. Enable the module as per usual.

2. Optional. To avoid sending any data at all to Google prior to the user
consenting to cookies, if you're using the Google Tag module, you can add the
Google Tag scripts to the "Disable JavaScripts" section on the EU Cookie
Compliance settings page here:

https://example.com/admin/config/system/eu-cookie-compliance/settings

In the **Disable the following JavaScripts when consent isn't given** section,
add the following to the _Disable JavaScripts_ setting:

```
modules/contrib/google_tag/js/gtm.js
modules/contrib/google_tag/js/gtag.js
modules/contrib/google_tag/js/gtag.ajax.js
```
